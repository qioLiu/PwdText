package com.sixsev.pwdtext

import android.content.Context
import android.graphics.*
import android.os.Handler
import android.os.Message
import android.text.InputType
import android.util.AttributeSet
import android.util.TypedValue
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.BaseInputConnection
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputConnection
import android.view.inputmethod.InputMethodManager

/**
 * DESCRIPTION
 * com.sixsev.pwdedittext
 * Created by six.sev on 2017/9/27.
 */
class PwdView : View{
    companion object {
        val DEFAULT_BORDER_COLOR = Color.parseColor("#000000")
        val DEFAULT_INPUT_BORDER_COLOR = Color.parseColor("#ff0000")
        val DEFAULT_BORDER_RADIUS = 5.0f
        val DEFAULT_PWD_LENGTH = 6
        val DEFAULT_PWD_TEXT_SIZE = 14.0f
        val DEFAULT_DELAY_TIME = 1000
        val DEFAULT_SHOW_TYPE = 3
        val DEFAULT_BORDER_WIDTH = 30.0f
        val DEFAULT_SPACE_WIDTH = 10.0f
        var invalidate = false
    }
    var borderColor = 0
    var inputBorderColor = 0
    var borderImg = -1
    var inputBorderImg = -1
    var borderRadius = 0f
    var pwdLength = 0
    var pwdTextColor = 0
    var pwdTextSize = 0
    var delayTime = 0
    var showType = 0
    var isShowSelected = false

    var dotRadius = 0f

    var borderWidth = 0
    var spaceWidth = 0
    var viewHeight = 0

    var saveResult: Int = 0

    var borderPaint: Paint? = null
    var inputBorderPaint: Paint? = null
    var pwdPaint: Paint? = null
    var dotPaint: Paint? = null

    var results: MutableList<Int>? = null

    var input: InputMethodManager? = null

    private var inputCallBack: InputCallBack? = null

    private var handler = object : Handler(){
        override fun handleMessage(msg: Message?) {
            super.handleMessage(msg)
            if (msg!!.what == 1) {
                invalidate = true
                invalidate()
            }
        }
    }

    constructor(context: Context?) : super(context){
        initStyle(context!!, null)
    }
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs){
        initStyle(context!!, attrs!!)
    }
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr){
        initStyle(context!!, attrs!!)
    }

    /**
     * 初始化属性值
     */
    fun initStyle(context: Context?, attrs: AttributeSet?){
        setOnKeyListener(NumberKeyListener())
        isFocusable = true
        isFocusableInTouchMode = true
        input = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        if(attrs == null){
            borderColor = DEFAULT_BORDER_COLOR
            inputBorderColor = DEFAULT_INPUT_BORDER_COLOR
            borderRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_BORDER_RADIUS, context!!.resources.displayMetrics)
            pwdLength = DEFAULT_PWD_LENGTH
            pwdTextColor =  DEFAULT_INPUT_BORDER_COLOR
            pwdTextSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, DEFAULT_PWD_TEXT_SIZE, context!!.resources.displayMetrics).toInt()
            delayTime =  DEFAULT_DELAY_TIME
            showType =  DEFAULT_SHOW_TYPE
        }else{
            val typedArray = context!!.obtainStyledAttributes(attrs, R.styleable.PwdView)
            borderColor = typedArray.getColor(R.styleable.PwdView_border_color, DEFAULT_BORDER_COLOR)
            inputBorderColor = typedArray.getColor(R.styleable.PwdView_input_border_color, DEFAULT_INPUT_BORDER_COLOR)
            borderImg = typedArray.getResourceId(R.styleable.PwdView_border_img, -1)
            inputBorderImg = typedArray.getResourceId(R.styleable.PwdView_input_border_img, -1)
            borderRadius = typedArray.getDimension(R.styleable.PwdView_border_radius, TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_BORDER_RADIUS, context!!.resources.displayMetrics))
            pwdLength = typedArray.getInteger(R.styleable.PwdView_pwd_length, DEFAULT_PWD_LENGTH)
            pwdTextColor = typedArray.getColor(R.styleable.PwdView_pwd_textColor, DEFAULT_INPUT_BORDER_COLOR)
            pwdTextSize = typedArray.getDimension(R.styleable.PwdView_pwd_textSize, TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, DEFAULT_PWD_TEXT_SIZE, context!!.resources.displayMetrics)).toInt()
            delayTime = typedArray.getInteger(R.styleable.PwdView_delayTime, DEFAULT_DELAY_TIME)
            showType = typedArray.getInteger(R.styleable.PwdView_showType, DEFAULT_SHOW_TYPE)
        }

        //取宽度和间隙的一个默认值
        borderWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_BORDER_WIDTH, context.resources.displayMetrics).toInt()
        spaceWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_SPACE_WIDTH, context.resources.displayMetrics).toInt()
        dotRadius = (borderWidth / 4).toFloat()
        results = mutableListOf<Int>()
        initPaints()
    }

    /**
     * 画笔初始化
     */
    private fun initPaints() {
        borderPaint = Paint()
        borderPaint!!.color = borderColor
        borderPaint!!.style = Paint.Style.STROKE
        borderPaint!!.isAntiAlias = true

        pwdPaint = Paint()
        pwdPaint!!.color = pwdTextColor
        pwdPaint!!.textSize = pwdTextSize.toFloat()
        pwdPaint!!.isAntiAlias = true

        dotPaint = Paint()
        dotPaint!!.color = pwdTextColor
        dotPaint!!.style = Paint.Style.FILL
        pwdPaint!!.isAntiAlias = true

        inputBorderPaint = Paint()
        inputBorderPaint!!.color = inputBorderColor
        inputBorderPaint!!.style = Paint.Style.STROKE
        pwdPaint!!.isAntiAlias = true
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        var widthSize = MeasureSpec.getSize(widthMeasureSpec)
        var heightSize = MeasureSpec.getSize(heightMeasureSpec)

        when(MeasureSpecMode@ widthMode){
            MeasureSpec.AT_MOST -> {
                when(MeasureSpecMode@ heightMode){
                    MeasureSpec.AT_MOST -> {
                        heightSize = borderWidth
                        widthSize = borderWidth.times(pwdLength) + spaceWidth.times(pwdLength - 1)
                        spaceWidth = borderWidth.div(4)
                    }
                    else -> {
                        borderWidth = heightSize
                        spaceWidth = borderWidth.div(4)
                        widthSize = borderWidth.times(pwdLength) + spaceWidth.times(pwdLength - 1)
                    }
                }
            }
            else -> {
                when(MeasureSpecMode@ heightMode){
                    MeasureSpec.AT_MOST -> {
                        borderWidth = widthSize.times(4).div(pwdLength.times(5))
                        heightSize = borderWidth
                        spaceWidth = borderWidth.div(4)
                    }
                    else ->{}
                }
            }
        }

        viewHeight = heightSize

        setMeasuredDimension(widthSize, viewHeight)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        drawBorder(canvas, viewHeight.toFloat())
        when (showType) {
            1 -> {
                for (index in 0..(results!!.size - 1)){
                    drawText(canvas, results!![index].toString(), index)
                    if (isShowSelected) {
                        val left = index.times(borderWidth.plus(spaceWidth)).plus(0.5.times(spaceWidth)).toFloat()
                        val right = (index.plus(1)).times(borderWidth).plus(index.times(spaceWidth)).plus(0.7.times(spaceWidth)).toFloat()
                        drawSelectBorder(canvas, left, right, viewHeight)
                    }
                }
            }
            2 -> {
                for (index in 0..(results!!.size - 1)){
                    val dotX = index.times(borderWidth.plus(spaceWidth)).plus(borderWidth.div(2)).plus(0.6.times(spaceWidth)).toFloat()
                    val dotY = height.div(2).toFloat()
                    canvas!!.drawCircle(dotX, dotY, dotRadius, dotPaint)
                    if (isShowSelected) {
                        val left = index.times(borderWidth.plus(spaceWidth)).plus(0.5.times(spaceWidth)).toFloat()
                        val right = (index.plus(1)).times(borderWidth).plus(index.times(spaceWidth)).plus(0.7.times(spaceWidth)).toFloat()
                        drawSelectBorder(canvas, left, right, viewHeight)
                    }
                }
            }
            else -> {
                if(invalidate){
                    drawDelayCircle(canvas)
                    return
                }
                for (index in 0..(results!!.size - 1)) {
                    val dotX = (index - 1).times(borderWidth.plus(spaceWidth)).plus(borderWidth.div(2)).plus(0.6.times(spaceWidth)).toFloat()
                    val dotY = height.div(2).toFloat()
                    val left = index.times(borderWidth.plus(spaceWidth)).plus(0.5.times(spaceWidth)).toFloat()
                    val right = (index.plus(1)).times(borderWidth).plus(index.times(spaceWidth)).plus(0.7.times(spaceWidth)).toFloat()
                    drawSelectBorder(canvas, left, right, viewHeight)
                    drawText(canvas, results!![index].toString(), index)
                    if (index + 1 == results!!.size) {
                        handler.sendEmptyMessageDelayed(1, delayTime.toLong())
                    }
                    //若按下back键保存的密码 > 输入的密码长度，则只绘制圆点
                    //即按下back键时，不绘制明文密码
                    /*if (!isShowTextPsw) {
                        if (saveResult > results!!.size) {
                            canvas!!.drawCircle(index.times(borderWidth.plus(spaceWidth)).plus(borderWidth.div(2)).plus(0.6.times(spaceWidth)).toFloat(), dotY, dotRadius, dotPaint);
                        }
                    }*/
                    if(index >= 1){
                        canvas!!.drawCircle(dotX, dotY, dotRadius, dotPaint)
                    }
                }
            }
        }
    }

    fun drawDelayCircle(canvas: Canvas?){
        invalidate = false
        for (index in 0..(results!!.size - 1)){
            val dotX = (index - 1).times(borderWidth.plus(spaceWidth)).plus(borderWidth.div(2)).plus(0.6.times(spaceWidth)).toFloat()
            val dotY = height.div(2).toFloat()
            val left = index.times(borderWidth.plus(spaceWidth)).plus(0.5.times(spaceWidth)).toFloat()
            val right = (index.plus(1)).times(borderWidth).plus(index.times(spaceWidth)).plus(0.7.times(spaceWidth)).toFloat()
            canvas!!.drawCircle(dotX, dotY, dotRadius, dotPaint)
            drawSelectBorder(canvas, left, right, viewHeight)
        }
        canvas!!.drawCircle((results!!.size - 1).times(borderWidth.plus(spaceWidth)).plus(borderWidth.div(2)).plus(0.6.times(spaceWidth)).toFloat(), height.div(2).toFloat(), dotRadius, dotPaint)
        handler.removeMessages(1)
    }

    /**
     * 绘制密码框
     */
    fun drawBorder(canvas: Canvas?, height: Float){
        var bitmap: Bitmap? = null
        var srcRect: Rect? = null
        if(isBorderImg()){
            bitmap = BitmapFactory.decodeResource(context.resources, borderImg)
            srcRect = Rect(0, 0, bitmap.width, bitmap.height)
        }
        for(index in 0..(pwdLength - 1)){
            var rect = RectF()
            val left = index.times(borderWidth.plus(spaceWidth)).plus(0.5.times(spaceWidth)).toFloat()
            val right = (index.plus(1)).times(borderWidth).plus(index.times(spaceWidth)).plus(0.7.times(spaceWidth)).toFloat()
            rect.set(left, 0f, right, height)
            if(bitmap == null) canvas!!.drawRoundRect(rect, borderRadius, borderRadius, borderPaint)
                                else
                                canvas!!.drawBitmap(bitmap, srcRect, rect, borderPaint)
        }
        if(bitmap != null && !bitmap!!.isRecycled) {
            bitmap!!.recycle()
        }
    }

    fun drawSelectBorder(canvas: Canvas?, left: Float, right: Float, height: Int){
        val rect_dest = RectF(left, borderPaint!!.strokeWidth, right, height - borderPaint!!.strokeWidth)
        when(inputBorderImg){
            -1 -> {
                canvas!!.drawRoundRect(rect_dest, borderRadius, borderRadius, inputBorderPaint)
            }
            else -> {
                val bitmap = BitmapFactory.decodeResource(context.resources, inputBorderImg)
                val rect_src = Rect(0, 0, bitmap.width, bitmap.height)
                canvas!!.drawBitmap(bitmap, rect_src, rect_dest, inputBorderPaint)
                if(bitmap != null && !bitmap!!.isRecycled)
                bitmap.recycle()
            }
        }
    }

    fun drawText(canvas: Canvas?, num: String, i: Int){
        var textRec: Rect = Rect()
        pwdPaint!!.getTextBounds(num, 0, num.length, textRec)
        val fontMetrics = pwdPaint!!.fontMetrics
        val textX = i.times(borderWidth.plus(spaceWidth)).plus((borderWidth.minus(textRec.width())).div(2)).plus(0.5.times(spaceWidth)).toFloat()
        val textY = (measuredHeight.minus(fontMetrics.bottom.minus(fontMetrics.top))).div(2).minus(fontMetrics.top)
        if (saveResult != 0 || saveResult < results!!.size) {
            canvas!!.drawText(num, textX, textY, pwdPaint)
        }
    }

    /**
     * 是否设置了密码框图片
     */
    fun isBorderImg(): Boolean{
        when(borderImg){
            -1 -> return false
            else -> return true
        }
    }

    interface InputCallBack {
        fun onInputFinish(password: String)
    }

    fun setInputCallBack(inputCallBack: InputCallBack) {
        this.inputCallBack = inputCallBack
    }

    /**
     * 清楚密码
     */
    fun clearPwd(){
        results!!.clear()
        invalidate()
    }

    /**
     * 获取密码字符串
     */
    fun getPwdString(): String{
        val stringBuilder = StringBuilder()
        val iterator = results!!.iterator()
        while (iterator.hasNext()){
            val next = iterator.next()
            stringBuilder.append(next)
        }
        return stringBuilder.toString()
    }

    fun hideKeyboard(){
        input!!.hideSoftInputFromInputMethod(windowToken, 0)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if(event!!.action == MotionEvent.ACTION_DOWN){
            requestFocus()
            input!!.showSoftInput(this, InputMethodManager.SHOW_FORCED)
            return true
        }
        return super.onTouchEvent(event)
    }

    override fun onWindowFocusChanged(hasWindowFocus: Boolean) {
        super.onWindowFocusChanged(hasWindowFocus)
        if(!hasWindowFocus) hideKeyboard()
    }

    override fun onCreateInputConnection(outAttrs: EditorInfo?): InputConnection {
        outAttrs!!.inputType = InputType.TYPE_CLASS_NUMBER
        outAttrs!!.imeOptions = EditorInfo.IME_ACTION_DONE
        return NumberInputConnection(this, false)
    }

    inner class NumberInputConnection(targetView: View?, fullEditor: Boolean) : BaseInputConnection(targetView, fullEditor) {
        override fun commitText(text: CharSequence?, newCursorPosition: Int): Boolean {
            return super.commitText(text, newCursorPosition)
        }

        override fun deleteSurroundingTextInCodePoints(beforeLength: Int, afterLength: Int): Boolean {
            if (beforeLength == 1 && afterLength == 0){
                return super.sendKeyEvent(KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_DEL))
                        && super.sendKeyEvent(KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_DEL))
            }
            return super.deleteSurroundingTextInCodePoints(beforeLength, afterLength)
        }
    }

    inner class NumberKeyListener : OnKeyListener{
        override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
            if (event!!.action == KeyEvent.ACTION_DOWN) {
                if (event.isShiftPressed) {//处理*#等键
                    return false
                }
                if (keyCode >= KeyEvent.KEYCODE_0 && keyCode <= KeyEvent.KEYCODE_9) {//只处理数字
                    if (results!!.size < pwdLength) {
                        results!!.add(keyCode - 7)
                        invalidate()
                        FinishInput()
                    }
                    return true
                }
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (!results!!.isEmpty()) {//不为空时，删除最后一个数字
                        saveResult = results!!.size
                        results!!.remove(results!![results!!.size - 1])
                        invalidate()
                    }
                    return true
                }
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    FinishInput()
                    return true
                }
            }
            return false
        }

        /**
         * 输入完成后调用的方法
         */
        fun FinishInput() {
            if (results!!.size == pwdLength && inputCallBack != null) {//输入已完成
                hideKeyboard()
                inputCallBack!!.onInputFinish(getPwdString())
            }
        }

    }
}